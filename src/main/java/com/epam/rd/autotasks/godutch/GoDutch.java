package com.epam.rd.autotasks.godutch;

import java.util.Scanner;

public class GoDutch {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int paymentAmount = scanner.nextInt();
        int amountOfFriends = scanner.nextInt();

        if (amountOfFriends <= 0) {
            System.out.println("Number of friends cannot be negative or zero");
            return;
        }

        int paymentWithTips = (int) (paymentAmount + (paymentAmount * 0.1));
        int amountToPay = paymentWithTips / amountOfFriends;

        if (paymentAmount < 0) {
            System.out.println("Bill total amount cannot be negative");
        } else {
            System.out.println(amountToPay);
        }

    }
}
